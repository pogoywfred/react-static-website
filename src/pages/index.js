import React from "react";

export default () => (
  <div style={{ textAlign: "center" }}>
    <h1>Welcome to React-Static</h1>
    <h2>Deployed via Gitlab CI</h2>
  </div>
);
